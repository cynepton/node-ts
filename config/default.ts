export default {
    port: 7000,
    host: 'localhost',
    dbUri: "mongodb://localhost:27017/rest-api"
}