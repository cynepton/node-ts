# Rest API with NodeJS and Typescript

## Starting a new NodeJS and Typescript RESTAPI

* Run `yarn init`
* Run `yarn add typescript`
* Run `npx tsc --init` to create `.tsconfig.json`
* Run `yarn add express yup config cors mongoose pino pino-pretty dayjs bcrypt jsonwebtoken lodash nanoid` to install dependencies
* Run `yarn add @types/body-parser @types/config @types/cors @types/express @types/node @types/yup @types/pino @types/mongoose @types/bcrypt @types/jsonwebtoken @types/lodash @types/nanoid ts-node typescript -D`